#!/usr/bin/env -S scala-cli shebang

//> using scala 3.5
//> using toolkit default
//> using option -new-syntax -explain -deprecation -feature -unchecked -Wunused:all

package ddiceware

import java.security.SecureRandom
import scala.jdk.StreamConverters._
import scala.math.pow

@main def main(
    wordlistFile: String,
    passphraseLength: Int,
    passphraseSeparator: String
): Unit =
  val wordlist = parseWordlist(wordlistFile)

  val indexOfSpecialWord = Random.random.nextInt(passphraseLength)
  val passphrase =
    (0 to passphraseLength)
      .map(i => {
        if i == indexOfSpecialWord then Random.digit.toString + Random.special
        else Random.word(wordlist)
      })
      .mkString(passphraseSeparator)
  println(s"Passphrase: $passphrase")

/** Parses a diceware wordlist from ``filename``. */
def parseWordlist(filename: String): Map[Int, String] =
  os.read.lines(os.Path(filename, os.pwd))
    .map(s => { (s.substring(0, 5).toInt, s.substring(6)) })
    .toMap

object Random:
  val random = SecureRandom()

  def word(wordlist: Map[Int, String]): String =
    val num = random
      .ints(5, 1, 7) // five random integers in the range 1 to 7
      .toScala(List) // IntStream (java) to List[Int] (scala)
      .zip(0 to 4) // zip-up an index
      .map((i, n) => {
        (i * pow(10, n)).toInt
      }) // shift digits in decimal system
      .sum // sum up to get diceware number
    wordlist(num)

  def digit: Int = random.nextInt(10)

  def special: Char =
    val punctuation = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
    punctuation.charAt(random.nextInt(punctuation.length))
