# ddiceware [![maintenance-status: as-is](https://img.shields.io/badge/maintenance--status-as--is-yellow)](https://gist.github.com/rusty-snake/574a91f1df9f97ec77ca308d6d731e29)

diceware passphrase generator.

## Getting started

### Prebuild jar

**Requirements:**

- Java 8 or newer
- a [dicware wordlist](#wordlist)

Download the latest release from https://codeberg.org/rusty-snake/ddiceware/releases
and start it like

```
java -jar ddiceware-assembly-0.0.3.jar wordlist.txt 5 _
```

### From source

**Requirements:**

- [scala CLI](https://scala-cli.virtuslab.org/)
- a [dicware wordlist](#wordlist)

```
scala-cli run ddiceware.scala -- <wordlist.txt> <passphrase length> <passphrase seperator>
```

## Wordlists

**English**

- https://www.eff.org/document/passphrase-wordlists

**German**

- https://github.com/dys2p/wordlists-de
- https://github.com/klamann/diceware-dereko
- https://gist.github.com/buchnema/8a62f81e54773d62a6f2d0c198b8798f
